<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221014200302 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE cliente_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE correo_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE departamento_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE direccion_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE documento_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE documentos_persona_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE municipio_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE sexo_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE categoria_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE clientes_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE producto_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE categoria (id INT NOT NULL, nombre VARCHAR(60) NOT NULL, descripcion VARCHAR(500) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE clientes (id INT NOT NULL, nombre VARCHAR(60) NOT NULL, apellido VARCHAR(60) DEFAULT NULL, direccion VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE producto (id INT NOT NULL, nombre VARCHAR(60) NOT NULL, precio DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE producto_categoria (producto_id INT NOT NULL, categoria_id INT NOT NULL, PRIMARY KEY(producto_id, categoria_id))');
        $this->addSql('CREATE INDEX IDX_B881A7077645698E ON producto_categoria (producto_id)');
        $this->addSql('CREATE INDEX IDX_B881A7073397707A ON producto_categoria (categoria_id)');
        $this->addSql('ALTER TABLE producto_categoria ADD CONSTRAINT FK_B881A7077645698E FOREIGN KEY (producto_id) REFERENCES producto (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE producto_categoria ADD CONSTRAINT FK_B881A7073397707A FOREIGN KEY (categoria_id) REFERENCES categoria (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cliente DROP CONSTRAINT fk_f41c9b25f5af7228');
        $this->addSql('ALTER TABLE correo DROP CONSTRAINT fk_77040bc97bf9ce86');
        $this->addSql('ALTER TABLE direccion DROP CONSTRAINT fk_f384be95de734e51');
        $this->addSql('ALTER TABLE direccion DROP CONSTRAINT fk_f384be9558bc1be0');
        $this->addSql('ALTER TABLE municipio DROP CONSTRAINT fk_fe98f5e05a91c08d');
        $this->addSql('ALTER TABLE documentos_persona DROP CONSTRAINT fk_1563c9a86601ba07');
        $this->addSql('ALTER TABLE documentos_persona DROP CONSTRAINT fk_1563c9a8de734e51');
        $this->addSql('DROP TABLE sexo');
        $this->addSql('DROP TABLE cliente');
        $this->addSql('DROP TABLE correo');
        $this->addSql('DROP TABLE direccion');
        $this->addSql('DROP TABLE municipio');
        $this->addSql('DROP TABLE documento');
        $this->addSql('DROP TABLE documentos_persona');
        $this->addSql('DROP TABLE departamento');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE categoria_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE clientes_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE producto_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE cliente_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE correo_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE departamento_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE direccion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE documento_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE documentos_persona_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE municipio_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE sexo_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE sexo (id INT NOT NULL, nombre VARCHAR(15) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE cliente (id INT NOT NULL, id_sexo_id INT DEFAULT NULL, edad INT DEFAULT NULL, primer_nombre VARCHAR(35) NOT NULL, segundo_nombre VARCHAR(35) DEFAULT NULL, primer_apellido VARCHAR(35) NOT NULL, segundo_apellido VARCHAR(35) DEFAULT NULL, fecha_nacimiento TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, fecha_creacion TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, fecha_actualizacion TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, telefono VARCHAR(12) DEFAULT NULL, celular VARCHAR(12) DEFAULT NULL, otros_documentos JSON DEFAULT NULL, activo BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_f41c9b25f5af7228 ON cliente (id_sexo_id)');
        $this->addSql('CREATE TABLE correo (id INT NOT NULL, id_cliente_id INT NOT NULL, email VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_77040bc97bf9ce86 ON correo (id_cliente_id)');
        $this->addSql('CREATE TABLE direccion (id INT NOT NULL, cliente_id INT NOT NULL, municipio_id INT DEFAULT NULL, descripcion VARCHAR(600) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_f384be9558bc1be0 ON direccion (municipio_id)');
        $this->addSql('CREATE INDEX idx_f384be95de734e51 ON direccion (cliente_id)');
        $this->addSql('CREATE TABLE municipio (id INT NOT NULL, departamento_id INT NOT NULL, nombre VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_fe98f5e05a91c08d ON municipio (departamento_id)');
        $this->addSql('CREATE TABLE documento (id INT NOT NULL, nombre VARCHAR(35) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE documentos_persona (id INT NOT NULL, id_documento_id INT NOT NULL, cliente_id INT NOT NULL, numero VARCHAR(25) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_1563c9a8de734e51 ON documentos_persona (cliente_id)');
        $this->addSql('CREATE INDEX idx_1563c9a86601ba07 ON documentos_persona (id_documento_id)');
        $this->addSql('CREATE TABLE departamento (id INT NOT NULL, nombre VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE cliente ADD CONSTRAINT fk_f41c9b25f5af7228 FOREIGN KEY (id_sexo_id) REFERENCES sexo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE correo ADD CONSTRAINT fk_77040bc97bf9ce86 FOREIGN KEY (id_cliente_id) REFERENCES cliente (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE direccion ADD CONSTRAINT fk_f384be95de734e51 FOREIGN KEY (cliente_id) REFERENCES cliente (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE direccion ADD CONSTRAINT fk_f384be9558bc1be0 FOREIGN KEY (municipio_id) REFERENCES municipio (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE municipio ADD CONSTRAINT fk_fe98f5e05a91c08d FOREIGN KEY (departamento_id) REFERENCES departamento (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE documentos_persona ADD CONSTRAINT fk_1563c9a86601ba07 FOREIGN KEY (id_documento_id) REFERENCES documento (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE documentos_persona ADD CONSTRAINT fk_1563c9a8de734e51 FOREIGN KEY (cliente_id) REFERENCES cliente (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE producto_categoria DROP CONSTRAINT FK_B881A7077645698E');
        $this->addSql('ALTER TABLE producto_categoria DROP CONSTRAINT FK_B881A7073397707A');
        $this->addSql('DROP TABLE categoria');
        $this->addSql('DROP TABLE clientes');
        $this->addSql('DROP TABLE producto');
        $this->addSql('DROP TABLE producto_categoria');
    }
}
