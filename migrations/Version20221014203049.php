<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221014203049 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE departamento_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE direccion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE municipio_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE departamento (id INT NOT NULL, nombre VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE direccion (id INT NOT NULL, municipio_id INT DEFAULT NULL, descripcion VARCHAR(600) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F384BE9558BC1BE0 ON direccion (municipio_id)');
        $this->addSql('CREATE TABLE municipio (id INT NOT NULL, departamento_id INT NOT NULL, nombre VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FE98F5E05A91C08D ON municipio (departamento_id)');
        $this->addSql('ALTER TABLE direccion ADD CONSTRAINT FK_F384BE9558BC1BE0 FOREIGN KEY (municipio_id) REFERENCES municipio (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE municipio ADD CONSTRAINT FK_FE98F5E05A91C08D FOREIGN KEY (departamento_id) REFERENCES departamento (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE clientes ADD direccion_completa_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE clientes ADD CONSTRAINT FK_50FE07D7666FDF2B FOREIGN KEY (direccion_completa_id) REFERENCES direccion (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_50FE07D7666FDF2B ON clientes (direccion_completa_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE clientes DROP CONSTRAINT FK_50FE07D7666FDF2B');
        $this->addSql('DROP SEQUENCE departamento_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE direccion_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE municipio_id_seq CASCADE');
        $this->addSql('ALTER TABLE direccion DROP CONSTRAINT FK_F384BE9558BC1BE0');
        $this->addSql('ALTER TABLE municipio DROP CONSTRAINT FK_FE98F5E05A91C08D');
        $this->addSql('DROP TABLE departamento');
        $this->addSql('DROP TABLE direccion');
        $this->addSql('DROP TABLE municipio');
        $this->addSql('DROP INDEX IDX_50FE07D7666FDF2B');
        $this->addSql('ALTER TABLE clientes DROP direccion_completa_id');
    }
}
