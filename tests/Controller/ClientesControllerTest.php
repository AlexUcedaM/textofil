<?php

namespace App\Test\Controller;

use App\Entity\Clientes;
use App\Repository\ClientesRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClientesControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private ClientesRepository $repository;
    private string $path = '/clientes/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->repository = (static::getContainer()->get('doctrine'))->getRepository(Clientes::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Cliente index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $originalNumObjectsInRepository = count($this->repository->findAll());

        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'cliente[nombre]' => 'Testing',
            'cliente[apellido]' => 'Testing',
            'cliente[direccion]' => 'Testing',
            'cliente[direccionCompleta]' => 'Testing',
        ]);

        self::assertResponseRedirects('/clientes/');

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Clientes();
        $fixture->setNombre('My Title');
        $fixture->setApellido('My Title');
        $fixture->setDireccion('My Title');
        $fixture->setDireccionCompleta('My Title');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Cliente');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Clientes();
        $fixture->setNombre('My Title');
        $fixture->setApellido('My Title');
        $fixture->setDireccion('My Title');
        $fixture->setDireccionCompleta('My Title');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'cliente[nombre]' => 'Something New',
            'cliente[apellido]' => 'Something New',
            'cliente[direccion]' => 'Something New',
            'cliente[direccionCompleta]' => 'Something New',
        ]);

        self::assertResponseRedirects('/clientes/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getNombre());
        self::assertSame('Something New', $fixture[0]->getApellido());
        self::assertSame('Something New', $fixture[0]->getDireccion());
        self::assertSame('Something New', $fixture[0]->getDireccionCompleta());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();

        $originalNumObjectsInRepository = count($this->repository->findAll());

        $fixture = new Clientes();
        $fixture->setNombre('My Title');
        $fixture->setApellido('My Title');
        $fixture->setDireccion('My Title');
        $fixture->setDireccionCompleta('My Title');

        $this->repository->add($fixture, true);

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertSame($originalNumObjectsInRepository, count($this->repository->findAll()));
        self::assertResponseRedirects('/clientes/');
    }
}
