<?php

namespace App\Entity;

use App\Repository\DireccionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DireccionRepository::class)
 * @ApiResource(
 *      collectionOperations={
 *          "get",
 *          "post"
 *      },
 *      normalizationContext=
 *          {"groups"={"norm"}},
 *      denormalizationContext=
 *          {"groups"={"post"}}
 * )
 */
class Direccion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({
     *      "post", "get", "norm" 
     *  })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=600)
     * @Groups({
     *      "post", "get", "norm" 
     *  })
     */
    private $descripcion;


    /**
     * @ORM\ManyToOne(targetEntity=Municipio::class)
     * @Groups({
     *      "post", "get", "norm" 
     *  })
     */
    private $municipio;

    /**
     * @ORM\OneToMany(targetEntity=Clientes::class, mappedBy="direccionCompleta")
     */
    private $clientes;

    public function __construct()
    {
        $this->clientes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }
  
    public function getMunicipio(): ?Municipio
    {
        return $this->municipio;
    }

    public function setMunicipio(?Municipio $municipio): self
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * @return Collection<int, Clientes>
     */
    public function getClientes(): Collection
    {
        return $this->clientes;
    }

    public function addCliente(Clientes $cliente): self
    {
        if (!$this->clientes->contains($cliente)) {
            $this->clientes[] = $cliente;
            $cliente->setDireccionCompleta($this);
        }

        return $this;
    }

    public function removeCliente(Clientes $cliente): self
    {
        if ($this->clientes->removeElement($cliente)) {
            // set the owning side to null (unless already changed)
            if ($cliente->getDireccionCompleta() === $this) {
                $cliente->setDireccionCompleta(null);
            }
        }

        return $this;
    }
}
