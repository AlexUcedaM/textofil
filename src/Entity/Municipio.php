<?php

namespace App\Entity;

use App\Repository\MunicipioRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MunicipioRepository::class)
 * @ApiResource(
 *      collectionOperations={
 *          "get",
 *          "post"
 *      },
 *      normalizationContext=
 *          {"groups"={"norm"}},
 *      denormalizationContext=
 *          {"groups"={"post"}}
 * )
 */
class Municipio
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({
     *       "get", "norm" ,"post"
     *  })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({
     *      "get", "norm" ,"post"
     *  })
     */
    private $nombre;

    /**
     * @Groups({
     *      "get", "norm" ,"post"
     *  })
     */
    private $nombreCompuesto;

    /**
     * @ORM\ManyToOne(targetEntity=Departamento::class, inversedBy="municipios")
     * @ORM\JoinColumn(nullable=false)
     */
    private $departamento;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }


    public function getNombreCompuesto(): ?string
    {
        $this->nombreCompuesto=  'Dep: '.$this->departamento->getNombre() .' - Mun:' .$this->nombre;
        return $this->nombreCompuesto;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDepartamento(): ?Departamento
    {
        return $this->departamento;
    }

    public function setDepartamento(?Departamento $departamento): self
    {
        $this->departamento = $departamento;

        return $this;
    }

    public function __toString()
    {
        return $this->departamento->getNombre() .' / '.$this->getNombre();
    }
}
